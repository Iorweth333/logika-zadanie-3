#include <iostream>
#include <string>
#include <fstream>

using namespace std;

bool read1 (char arr[9][3], string filename)
{
    ifstream in_file;
    string file_path = "./";
    file_path = file_path + filename;
    in_file.open (file_path.c_str());   //c_str allows to pass string to function taking char*

    int Nverse = 0; //number of verse
    string line;   //content of verse
    unsigned int i,k;
    while (in_file.good())
    {

        if (Nverse > 8)
        {
            cout<<"plik "<<filename<<" zawiera zbyt wiele wierszy\n";
            cout<<"jesli w wierszach ponizej dziewiatego \n";
            cout<<"sa istotne informacje, to nie zostana wczytane\n";
        }


        getline (in_file, line);


        i=0;    //index of sign in the line
        k=0;    //index of column in array

        while (i < line.size() and k<3)
        {
            if (line [i] != ' ')
            {
                if (line [i] != '1' and line [i] != '0' and line [i] != 'X')
                {
                    cout<<"Niewlasciwy znak w pliku "<<filename<<"\n";
                    return false;
                }
                else
                {
                    arr [Nverse][k] = line [i];
                    k++;
                }
            }
            i++;
        }

        Nverse++;
    }
    return true;

}

bool read2 (char arr[3][2], string filename)    //second function doing exactly the same thing, but the size of array is different. I am not proud of it...
{
    ifstream in_file;
    string file_path = "./";
    file_path = file_path + filename;
    in_file.open (file_path.c_str());   //c_str allows to pass string to function taking char*

    int Nverse = 0; //number of verse
    string line;   //content of verse
    unsigned int i,k;

    while (in_file.good())
    {

        if (Nverse > 2)
        {
            cout<<"plik "<<filename<<" zawiera zbyt wiele wierszy\n";
            cout<<"jesli w wierszach ponizej trzeciego \n";
            cout<<"sa istotne informacje, to nie zostana wczytane\n";
        }


        getline (in_file, line);


        i=0;    //index of sign in the line
        k=0;    //index of column in array

        while (i < line.size() and k<2)
        {
            if (line [i] != ' ')
            {
                if (line [i] != '1' and line [i] != '0' and line [i] != 'X')
                {
                    cout<<"Niewlasciwy znak w pliku "<<filename<<"\n";
                    return false;
                }
                else
                {
                    arr [Nverse][k] = line [i];
                    k++;
                }
            }
            i++;
        }

        Nverse++;
    }
    return true;

}

string answers_interpretation (string answers[10], int n)
{
    string interpreted;

    for (int i = 0; i<n; i++)
    {
        if (answers [i] == "and")
        {
            interpreted += "a";
            continue;
        }
        if (answers [i] == "or")
        {
            interpreted += "o";
            continue;
        }
        if (answers [i] == "not")
        {
            interpreted += "n";
            continue;
        }
        if (answers [i] == "impl")
        {
            interpreted += "i";
            continue;
        }
        if (answers [i] == "yes")
        {
            interpreted += "y";
            continue;
        }
        // if no "continue" worked, then incorrect answer
        interpreted = "error";
        break;

    }

    return interpreted;
}

bool check_answers (char element1, char element2, char answer_expected, char which_arr, char andd [9][3], char orr [9][3], char impl [9][3])
{
    if (which_arr == 'a')
    {
        for (int i = 0; i<9; i++)
            if (andd[i][0] == element1 and andd[i][1] == element2 and andd[i][2] == answer_expected) return true;
    }

    if (which_arr == 'o')
    {
        for (int i = 0; i<9; i++)
            if (orr[i][0] == element1 and orr[i][1] == element2 and orr[i][2] == answer_expected) return true;
    }

    if (which_arr == 'i')
    {
        for (int i = 0; i<9; i++)
            if (impl[i][0] == element1 and impl[i][1] == element2 and impl[i][2] == answer_expected) return true;
    }

    return false;
}

char simplification (char element1, char element2, char which_arr, char andd[9][3], char orr [9][3], char impl [9][3])
{
    string arr_name;
    if (which_arr == 'a')
    {
        arr_name = "and";
        for (int i = 0; i<9; i++)
            if (andd[i][0] == element1 and andd[i][1] == element2) return andd [i][2];
    }
    if (which_arr == 'o')
    {
        arr_name = "or";
        for (int i = 0; i<9; i++)
            if (orr[i][0] == element1 and orr[i][1] == element2) return orr [i][2];
    }
    if (which_arr == 'i')
    {
        arr_name = "impl";
        for (int i = 0; i<9; i++)
            if (impl[i][0] == element1 and impl[i][1] == element2) return impl [i][2];
    }

    //if nothing returned yet, the array does not include such combination of elements
    //which means it's incorrectly built.


    cout << "Tabela " << arr_name<< " jest niepoprawnie zbudowana. \nNie zawiera kombinacji wejsciowej "<<element1<<element2<<"\n";
    return 'e';
}

void negation (char &element, char arr[3][2], string arr_name)
{
    bool found = false;
    int i=0;
    while ( i<3 and !found)
    {
        if (arr[i][0] == element)
        {
            element = arr[i][1];
            found = true;
        }
        i++;
    }

    //if flag not true, the array does not include such element
    //which means it's incorrectly built.
    if (!found)
    {
        cout << "Tabela " << arr_name<< " jest niepoprawnie zbudowana. \nNie zawiera elemntu wejsciowego: "<<element<<"\n";
        element = 'e';
    }

}

int main (int argc, char * argv[])
{
	if (argc != 5)
	{
		cout<<"Niewlasciwa ilosc argumentow! \nProgram potrzebuje czterech plikow konfiguracyjnych. \nPodaj ich pelne nazwy przy nastepnym wywolaniu.\n";
		return 1;
	}

	string argument;
	int i;
	char andd [9][3];   //doubled last letter
	char impl [9][3];    //because these are
	char orr [9][3];    //key words in c++
	char nott [3][2];   //(excluding impl)

	int file_error = 0;

	for (i=1; i<=4; i++)
    {
        argument = argv[i];
        if (argument == "and.txt")
        {
            if ( !read1 (andd, "and.txt"))
                {
                    cout<<"Blad przy wczytaniu pliku and.txt";
                    return 1;
                }
            continue;
        }

        if (argument == "impl.txt")
        {
            if ( !read1 (impl, "impl.txt"))
                {
                    cout<<"Blad przy wczytaniu pliku impl.txt";
                    return 1;
                }
            continue;
        }


        if (argument == "or.txt")
        {
            if ( !read1 (orr, "or.txt"))
                {
                    cout<<"Blad przy wczytaniu pliku or.txt";
                    return 1;
                }
            continue;
        }

        if (argument == "not.txt")
        {
             if ( !read2 (nott, "not.txt"))
                {
                    cout<<"Blad przy wczytaniu pliku not.txt";
                    return 1;
                }
            continue;
        }

        file_error++;   //if none of files' names match
                        //then error message must show
    }

    if (file_error != 0)
    {
        cout<<"Nazwa ktoregos z plikow jest niepoprawna\n";
        cout<<"Oczekiwano plikow: and.txt, or.txt, impl.txt, not.txt \n";
        return 1;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//START
    cout << "\nWitamy w grze Polscy Logicy! \nDwoch polskich logikow postanowilo sprawdzic swoje umiejetnosci \n \n";
    cout << "Jestes jednym z nich! \nZagracie w logiczna gre. Zasady sa nastepujace: \n";
    cout << "Twoj przeciwnik bedzie ci zadawal zagadki. \nDobierz spojniki i negacje tak, aby uzyskac zadana wartosc logiczna.\n";
    cout << "Wpisz and, or lub impl aby wstawic spojnik w miejsce kropek. \nW miejsce kresek mozna wstawic negacje. Jesli nie chcesz negowac, wpisz yes. \n";
    cout << "\nWAZNE! Warto oddzielac kolejne spojniki/negacje klawiszem ENTER. Pomaga to zapobiec bledom.\n\n";
    cout << "Za kazda poprawna odpowiedz otrzymasz jeden punkt \n \n";

    int score = 0;
    string answers [10];
    string answers_interpreted;
    char ingredients [5];


    QUESTION1:
    ingredients[0] = '1';
    ingredients[1] = '1';
    cout << "Na poczatek cos prostego. \nOczekiwany wynik: PRAWDA. \n";
    cout << "___3 > 2  ...  ___log (2) < log (5) \n";                   // ___1 ... ___1 -> 1
    cout << "\nPrzykladowa odpowiedz: \nyes [ENTER] impl [ENTER] not [ENTER] \n";


    for (int i = 0; i<3; i++) cin>> answers[i];

    answers_interpreted = answers_interpretation (answers, 3);

    if (answers_interpreted[0] == 'n') negation (ingredients[0], nott, "not");
    if (answers_interpreted[2] == 'n') negation (ingredients[1], nott, "not");

    if (answers_interpreted [1] == 'y' or answers_interpreted[1] == 'n') answers_interpreted = "error";

    if (answers_interpreted == "error")
    {
        cout << "Wykryto nieszablonowa odpowiedz. \nOdpowiedziami moga byc slowa: and, or, impl, not, yes \n";
        cout << "Not oraz yes moga byc tylko na miejscach oznaczonych kreskami.\nSprobuj jeszcze raz\n";
        goto QUESTION1;
    }

    if (check_answers (ingredients[0], ingredients[1], '1', answers_interpreted [1], andd, orr, impl))
    {
        cout<<"Poprawna odpowiedz, gratulacje! Zdobywasz punkt.\n \n";
        score++;
    }
    else cout<<"Niestety nie jest to poprawna odpowiedz. \n \n";

    for (int i = 0; i<10; i++) answers[i].clear();   //clearing answers. no need to clear answers_interpreted

    QUESTION2:

    ingredients [0] = '0';
    ingredients [1] = '1';
    ingredients [2] = '1';
    cout << "Teraz cos powazniejszego. \nOczekiwany wynik: FALSZ. \n";
    cout << "( ___sin 10 > cos 10 ... ___4^3 < 3^4 ) ...  ___NWD (45,60) = 15 [katy w stopniach] \n"; // (___0 ... ___1) ... ___1 -> 0

    for (int i = 0; i<5; i++) cin>> answers[i];
    answers_interpreted = answers_interpretation (answers,5);

    if (answers_interpreted[0] == 'n') negation (ingredients[0], nott, "not");
    if (answers_interpreted[2] == 'n') negation (ingredients[1], nott, "not");
    if (answers_interpreted[4] == 'n') negation (ingredients[2], nott, "not");

    if (answers_interpreted [1] == 'y' or answers_interpreted[1] == 'n') answers_interpreted = "error";
    if (answers_interpreted [3] == 'y' or answers_interpreted[3] == 'n') answers_interpreted = "error";


    if (answers_interpreted == "error")
    {
        cout << "Wykryto nieszablonowa odpowiedz. \nOdpowiedziami moga byc slowa: and, or, impl, not, yes \n";
        cout << "Not oraz yes moga byc tylko na miejscach oznaczonych kreskami.\nSprobuj jeszcze raz\n";
        goto QUESTION2;
    }

    ingredients [1] = simplification (ingredients [0], ingredients[1], answers_interpreted [1], andd, orr, impl);

    if (ingredients [1] == 'e') //if error occurred...
    {
        cout << "Napotkano na blad. Nastapi wylaczenie gry \n";
        return 1;
    }


    if (check_answers (ingredients [1], ingredients [2], '0', answers_interpreted [3], andd, orr, impl))
    {
        cout<<"Poprawna odpowiedz, gratulacje! Zdobywasz punkt.\n \n";
        score++;
    }
    else cout<<"Niestety nie jest to poprawna odpowiedz. Pamietasz, ze oczekiwano falszu? \n \n";

    for (int i = 0; i<10; i++) answers[i].clear();   //clearing answers. no need to clear answers_interpreted

    QUESTION3:
    ingredients[0] = '1';
    ingredients[1] = '0';
    cout << "A teraz uwaznie! Oczekiwany wynik: PRAWDA \n";
    cout<< "___arcsin (0) = sin (0)  ... ___4 = 4! \n";     //___1 ... ___0

    for (int i = 0; i<3; i++) cin>> answers[i];
    answers_interpreted = answers_interpretation (answers,3);

    if (answers_interpreted[0] == 'n') negation (ingredients[0], nott, "not");
    if (answers_interpreted[2] == 'n') negation (ingredients[1], nott, "not");

    if (answers_interpreted[1] == 'n' or answers_interpreted[1] == 'y') answers_interpreted="error";

    if (answers_interpreted == "error")
    {
        cout << "Wykryto nieszablonowa odpowiedz. \nOdpowiedziami moga byc slowa: and, or, impl, not, yes \n";
        cout << "Not oraz yes moga byc tylko na miejscach oznaczonych kreskami.\nSprobuj jeszcze raz\n";
        goto QUESTION3;
    }

    if (check_answers (ingredients [0], ingredients [1], '1', answers_interpreted [1], andd, orr, impl))
    {
        cout<<"Poprawna odpowiedz, gratulacje! Zdobywasz punkt.\n \n";
        score++;
    }
    else cout<<"Niestety nie jest to poprawna odpowiedz. Czyzbys nie zauwazyl silni? \n \n";

    for (int i = 0; i<10; i++) answers[i].clear();   //clearing answers. no need to clear answers_interpreted

    QUESTION4:
    ingredients [0]='X';
    ingredients [1]='1';
    cout << "Moze troche z innej beczki? Oczekiwany wynik: WARTOSC NIEZNANA \n";
    cout << "___(4 + 3i) > (2 + 2i) ... Fib (6) = 8 [Zalozmy ze caig Fib. zaczyna sie od zera i dwoch jedynek] \n"; //___X ... ___1

    for (int i = 0; i< 3; i++) cin>> answers[i];
    answers_interpreted = answers_interpretation (answers,2);

    if (answers_interpreted[0] == 'n') negation (ingredients[0], nott, "not");
    if (answers_interpreted[2] == 'n') negation (ingredients[1], nott, "not");

    if (answers_interpreted[1] == 'n' or answers_interpreted[1] == 'y') answers_interpreted="error";

    if (answers_interpreted == "error")
    {
        cout << "Wykryto nieszablonowa odpowiedz. \nOdpowiedziami moga byc slowa: and, or, impl, not, yes \n";
        cout << "Not oraz yes moga byc tylko na miejscach oznaczonych kreskami.\nSprobuj jeszcze raz\n";
        goto QUESTION4;
    }

    if (check_answers (ingredients [0], ingredients [1], 'X', answers_interpreted [1], andd, orr, impl))
    {
        cout<<"Poprawna odpowiedz, gratulacje! Zdobywasz punkt.\n \n";
        score++;
    }
    else cout<<"Niestety nie jest to poprawna odpowiedz. Nalezy pamietac, ze operatory porownania nie sa zdefiniowane w zbiorze liczb zespolonych! \n \n";

    for (int i = 0; i<10; i++) answers[i].clear();   //clearing answers. no need to clear answers_interpreted


    QUESTION5:
    ingredients [0] = 'X';
    ingredients [1] = '1';
    ingredients [2] = 'X';

    cout << "Lecimy dalej. Oczekiwany wynik: PRAWDA \n";
    cout << "___sin (x) < cos (x) ... [ ___(4 + i) - (2 - 2i) = (2 +3i) ... ___AGH wymiata! ] \n"; //___X ... ( ___1 ... ___X)

    for (int i = 0; i< 5; i++) cin>> answers[i];
    answers_interpreted = answers_interpretation (answers,5);

    if (answers_interpreted[0] == 'n') negation (ingredients[0], nott, "not");
    if (answers_interpreted[2] == 'n') negation (ingredients[1], nott, "not");
    if (answers_interpreted[4] == 'n') negation (ingredients[2], nott, "not");

    if (answers_interpreted[1] == 'n' or answers_interpreted[1] == 'y') answers_interpreted="error";
    if (answers_interpreted[3] == 'n' or answers_interpreted[3] == 'y') answers_interpreted="error";

    if (answers_interpreted == "error")
    {
        cout << "Wykryto nieszablonowa odpowiedz. \nOdpowiedziami moga byc slowa: and, or, impl, not, yes \n";
        cout << "Not oraz yes moga byc tylko na miejscach oznaczonych kreskami.\nSprobuj jeszcze raz\n";
        goto QUESTION5;
    }

    ingredients [1] = simplification (ingredients [1], ingredients[2], answers_interpreted [3], andd, orr, impl);

    if (ingredients [1] == 'e') //if error occurred...
    {
        cout << "Napotkano na blad. Nastapi wylaczenie gry \n";
        return 1;
    }

    if (check_answers (ingredients [0], ingredients [1], '1', answers_interpreted [1], andd, orr, impl))
    {
        cout<<"Poprawna odpowiedz, gratulacje! Zdobywasz punkt.\n \n";
        score++;
    }
    else
    {
        cout<<"Niestety nie jest to poprawna odpowiedz.\n";
        cout<<"O ile w zespolonych nie ma porownan to dodawanie i odejmowanie juz jest. \nCo do AGH... polemizowac ciezko ;), ale co gust to inny, takze wartosc nieznana \n";
    }


    for (int i = 0; i<10; i++) answers[i].clear();   //clearing answers. no need to clear answers_interpreted

    cout << "Twoj wynik to: "<<score<<"\n\n";
    if (score < 3) cout<<"Oj bratku, wiecej szlifow! \n";
    if (score == 3 ) cout <<"Calkiem niezle! \n";
    if (score > 3) cout <<"Brawo! Jestes Logiczna Machina Zaglady! \n\n";
	return 0;
}
