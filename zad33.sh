#! /bin/bash

#KAROL BIELEN
#VERSION WITH NEGATED IF
#this program answers a question: did A team won in the game?
#this game is fictional, imaginated by me 
#two teams, A and B, score three kinds of points: 
#	development points(PTS), civilisation echievements(CA) and strategic points(SP)
#wins the team, which scores more points in at least two categories
#scores of these teams are given by the user as arguments
#sequence of arguments: PTS A, PTS B, CA A, CA B, SP A, SP B
	
if [ $# -lt 6 ]; then
	echo "Za malo argumentow. Podaj 6 liczb: dla obu druzyn liczbe punktow, odkryc cywilizacji i liczbe zdobytych punktow strategicznych (w tej konkretnej kolejnosci, naprzemian dla obu druzyn"
	else	
		propriety=1 #will be useful to check if arguments are ok
		noop=0  #no operation- it will be useful later
		aPTS=$1 #development points
		bPTS=$2
		aCA=$3 #civilisation achievements
		bCA=$4
		aSP=$5 #strategic points
		bSP=$6
		
		
		for i in $* 
		do	 
			if [ $i -ge 0 ] 
			then
				  noop=1 #i know that this if is weird, but it didn't work in any other way I invented. instruction noop=1 is there only to get through the parser 
			else
				echo "Argument $i jest niepoprawny. Argumenty musza byc liczbami naturalnymi!"
				propriety=0
			fi
		done
		
		if [ $propriety -eq 1 ]; then

			if   ( [ $aSP -le $bSP ] || [ $aCA -le $bCA ] ) && ( [ $aSP -le $bSP ] || [ $aPTS -le $bPTS ] ) && ( [ $aCA -le $bCA ] || [ $aPTS -le $bPTS ] )
			then	
				echo "druzyna A wygrala"
			else
				echo "druzyna A nie wygrala"
			fi
		fi
fi

