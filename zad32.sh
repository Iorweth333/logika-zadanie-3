#! /bin/bash

#KAROL BIELEN	

#script checking other script's output

propriety=1	#I assume that everything is ok. propriety will be changed to 0 if error detected

if [ $# -eq 0 ]; then
	echo "Nieprawidlowe wywolanie. Podaj nazwe testowanego programu (tylko nazwe, bez sciezki)"
else
	if [ -e ./$1 ]; then
	
	#draw:
	#CASE1 draw draw draw
	output=`./$1 1 1 1 1 1 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 1 Wprowadzono dane: 1 1 1 1 1 1. Nieprawidlowa odpowiedz."
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi

	# the A team wins (in one category or more, not necessarily in whole game):
	#CASE2 A A A
	output=`./$1 2 1 2 1 2 1 `
	if [[ $output != "druzyna A wygrala" ]]
		then echo "BLAD 2 Wprowadzono dane: 2 1 2 1 2 1. Nieprawidlowa odpowiedz. "
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A wygrala"
		propriety=0
	fi

	#CASE3 A draw draw
	output=`./$1 2 1 1 1 1 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 3 Wprowadzono dane: 2 1 1 1 1 1. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE4 draw A draw
	output=`./$1 1 1 2 1 1 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 4 Wprowadzono dane: 1 1 2 1 1 1. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE5 draw draw A
	output=`./$1 1 1 1 1 2 1  `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 5 Wprowadzono dane: 1 1 1 1 2 1. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE6 A A draw
	output=`./$1 2 1 2 1 1 1 `
	if [[ $output != "druzyna A wygrala" ]]
		then echo "BLAD 6 Wprowadzono dane: 2 1 2 1 1 1. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A wygrala"
		propriety=0
	fi
	
	#CASE7 A draw A
	output=`./$1 2 1 1 1 2 1 `
	if [[ $output != "druzyna A wygrala" ]]
		then echo "BLAD 7 Wprowadzono dane: 2 1 1 1 2 1. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A wygrala"
		propriety=0
	fi	

	#CASE8 draw A A
	output=`./$1 1 1 2 1 2 1 `
	if [[ $output != "druzyna A wygrala" ]]
		then echo "BLAD 8 Wprowadzono dane: 1 1 2 1 2 1. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A wygrala"
		propriety=0
	fi
	
	#CASE9 A A B
	output=`./$1 2 1 2 1 1 2 `
	if [[ $output != "druzyna A wygrala" ]]
		then echo "BLAD 9 Wprowadzono dane: 2 1 2 1 1 2. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A wygrala"
		propriety=0
	fi
	
	#CASE10 A B A
	output=`./$1 2 1 1 2 2 1 `
	if [[ $output != "druzyna A wygrala" ]]
		then echo "BLAD 10 Wprowadzono dane: 2 1 1 2 2 1. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A wygrala"
		propriety=0
	fi
	
	#CASE11 B A A
	output=`./$1 1 2 2 1 2 1 `
	if [[ $output != "druzyna A wygrala" ]]
		then echo "BLAD 11 Wprowadzono dane: 1 2 2 1 2 1. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A wygrala"
		propriety=0
	fi
	
	###########################################################################
	#the B team wins:
	#CASE12 B B B
	output=`./$1 1 2 1 2 1 2 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 12 Wprowadzono dane: 1 2 1 2 1 2. Nieprawidlowa odpowiedz."
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	
	#CASE13 B draw draw
	output=`./$1 1 2 1 1 1 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 13 Wprowadzono dane: 1 2 1 1 1 1. Nieprawidlowa odpowiedz." 
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE14 draw B draw
	output=`./$1 1 1 1 2 1 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 14 Wprowadzono dane: 1 1 1 2 1 1. Nieprawidlowa odpowiedz. "
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE15 draw draw B
	output=`./$1 1 1 1 1 1 2 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 15 Wprowadzono dane: 1 1 1 1 1 2. Nieprawidlowa odpowiedz. "
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE16 B B draw
	output=`./$1 1 2 1 2 1 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 16 Wprowadzono dane: 1 2 1 2 1 1. Nieprawidlowa odpowiedz. "
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE17 B draw B
	output=`./$1 1 2 1 1 1 2 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 17 Wprowadzono dane: 1 2 1 1 1 2. Nieprawidlowa odpowiedz. "
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi

	#CASE18 draw B B
	output=`./$1 1 1 1 2 1 2 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 18 Wprowadzono dane: 1 1 1 2 1 2. Nieprawidlowa odpowiedz. "
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE19 B B A
	output=`./$1 1 2 1 2 2 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 19 Wprowadzono dane: 1 2 1 2 2 1. Nieprawidlowa odpowiedz. "
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE20  B A B
	output=`./$1 1 2 2 1 1 2 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 20 Wprowadzono dane: 1 2 2 1 1 2. Nieprawidlowa odpowiedz. "
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE21 A B B
	output=`./$1 2 1 1 2 1 2 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 21 Wprowadzono dane: 2 1 1 2 1 2. Nieprawidlowa odpowiedz. "
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	################################################################
	#other draws:
	#CASE22 draw A B
	output=`./$1 1 1 2 1 1 2 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 22 Wprowadzono dane: 1 1 2 1 1 2. Nieprawidlowa odpowiedz."
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE23 draw B A
	output=`./$1 1 1 1 2 2 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 23 Wprowadzono dane: 1 1 1 2 2 1. Nieprawidlowa odpowiedz."
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE24 A draw B
	output=`./$1 2 1 1 1 1 2 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 24 Wprowadzono dane: 2 1 1 1 1 2. Nieprawidlowa odpowiedz."
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE25 B draw A
	output=`./$1 1 2 1 1 2 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 25 Wprowadzono dane: 1 2 1 1 2 1. Nieprawidlowa odpowiedz."
		echo	 "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
	propriety=0
	fi

	#CASE26 A B draw
	output=`./$1 2 1 1 2 1 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 26 Wprowadzono dane: 2 1 1 2 1 1. Nieprawidlowa odpowiedz."
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	#CASE27 B A draw
	output=`./$1 1 2 2 1 1 1 `
	if [[ $output != "druzyna A nie wygrala" ]]
		then echo "BLAD 27 Wprowadzono dane: 1 2 2 1 1 1. Nieprawidlowa odpowiedz."
		echo "Otrzymano odpowiedz: $output"
		echo "Spodziewano odpowiedzi: druzyna A nie wygrala"
		propriety=0
	fi
	
	################################################################
	#finish:
	if [ $propriety -eq 1 ]
		then echo "All tests passed correctly, everything is OK"
	fi
	
	
	fi	#end of "if's" checking if there was good argument given
fi		
		
		

	









